package ru.tsc.karbainova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.karbainova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/tasks")
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Task create(@RequestBody final Task task) {
        taskService.save(task);
        return task;
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public Task save(@RequestBody final Task task) {
        taskService.save(task);
        return task;
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        taskService.removeById(id);
    }
}
