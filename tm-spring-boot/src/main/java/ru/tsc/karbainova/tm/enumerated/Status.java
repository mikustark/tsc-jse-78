package ru.tsc.karbainova.tm.enumerated;

public enum Status {
    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static Status getStatus(final String status) {
        if(status.equals(COMPLETED.displayName)) return COMPLETED;
        if(status.equals(IN_PROGRESS.displayName)) return IN_PROGRESS;
        return NOT_STARTED;
    }


}
