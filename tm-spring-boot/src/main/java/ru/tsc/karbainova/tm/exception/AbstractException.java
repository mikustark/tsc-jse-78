package ru.tsc.karbainova.tm.exception;

public class AbstractException extends RuntimeException {
    public AbstractException(String message) {
        super(message);
    }
}
