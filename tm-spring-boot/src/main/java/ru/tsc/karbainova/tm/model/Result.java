package ru.tsc.karbainova.tm.model;

import lombok.Getter;

@Getter
public class Result {

    private Boolean success = true;
    public String message = null;

    public Result() {
    }

    public Result(Boolean success) {
        this.success = success;
    }

    public Result(Exception e) {
        success = false;
        message = e.getMessage();
    }
}
